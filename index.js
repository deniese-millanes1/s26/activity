const http = require("http");

const port = 3000;

const server = http.createServer((request, respond) => {

	if(request.url == "/login") {
		respond.writeHead(200, {"Content-Type": "text/plain"});
		respond.end("You Are Currently At The Log In Page");
	} else {
		respond.writeHead(404, {"Content-Type": "text/plain"});
		respond.end("Page Not Available");
	}
});

server.listen(port);

console.log(`The server is successfully running`);